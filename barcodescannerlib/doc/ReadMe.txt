使用说明:
       1、把模块 barcodescannerlib 作为你的项目依赖添加到工程中.
       2、在你的 AndroidManifest.xml 文件中添加权限
        <uses-permission android:name="android.permission.CAMERA" />
        <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
        <uses-feature android:name="android.hardware.camera" />
        <uses-feature android:name="android.hardware.camera.autofocus" />
        <uses-permission android:name="android.permission.VIBRATE" />
        <uses-permission android:name="android.permission.FLASHLIGHT" />
        别忘了同时在 AndroidManifest.xml 中声明 CaptureActivity 这个Activity.
        (eg:
        <activity
                 android:name="com.zero.barcodescannerlib.CaptureActivity"
                 android:configChanges="orientation|keyboardHidden"
                 android:screenOrientation="portrait"
                 android:theme="@style/AppTheme"
                 android:windowSoftInputMode="stateAlwaysHidden"/>)
       3、 代码中调用 eg: startActivity(new Intent(this, CaptureActivity.class));




参考文档 : http://my.oschina.net/madmatrix/blog/189031
          http://www.cnblogs.com/keyindex/archive/2011/06/08/2074900.html
